import { numberCheck } from "../src/number";


describe ('number_type', () => {

    it ('Es par', ()=>{
        const a = 22;
        expect(numberCheck(a)).toBe(true);
    })

    it ('Es impar', ()=>{
        const a = 23;

        expect(numberCheck(a)).toBe(false);
    })

    it('Is not a number', ()=>{
        const a = "number";
        expect(numberCheck(a)).toBe("Not a number");
    })

})