import { palindrome } from "../src/palindrome"

describe( 'Palabras palíndromo',() => {

    it('Es palíndromo', () => {
        const text = 'Logra Casillas, allí sacar gol';

        expect(palindrome(text)).toBe(true);
    })

    it('No es palíndromo', ()=>{
        const text = 'Cualquier frase casi que se te ocurra';

        expect(palindrome(text)).toBe(false)
    })
})