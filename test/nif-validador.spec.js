import { lenght_check, letter_check } from "../src/nif-validator";



describe ('Validate NIF',() => {
    it('Letter validator', () =>{
        const nifLetter = "12345678Z"
        expect(letter_check(nifLetter)).toBe('Z')

    });

    it('Validate lenght', () => {
        const nifLenght = "12345678Z"
        expect(lenght_check(nifLenght)).toBe(true);
    })

})