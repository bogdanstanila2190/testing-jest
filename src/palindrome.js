export function palindrome (text){
    
    
    const remove_accent = text.normalize("NFD").replace(/([\u0300-\u036f]|[^0-9a-zA-Z])/g, '').toLowerCase()
    
    if (remove_accent.split('').reverse().join('') == remove_accent){
        return true
    }else{
        return false
    }
    
}
